package com.kyjg.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HistoryInsuranceCorporationItem {
    private String patientName;
    private String patientPhone;
    private String registrationNumber;
    private String medicalItemName;
    private Double medicalItemNonSalaryPrice;
    private Double patientContributionPrice;
    private Double billingAmount;
    private LocalDate dateCure;
}
