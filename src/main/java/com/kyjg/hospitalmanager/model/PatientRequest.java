package com.kyjg.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PatientRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String patientName;
    @NotNull
    @Length(min = 13, max = 13)
    private String patientPhone;
    @NotNull
    @Length(min = 14, max = 14)
    private String registrationNumber;
    @NotNull
    @Length(min = 3, max = 100)
    private String patientAddress;
    @NotNull
    @Length(min = 2)
    private String memo;
}
