package com.kyjg.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class HistoryItem {
    private Long historyId;
    private Long patientId;
    private String patientName;
    private String patientPhone;
    private String registrationNumber;
    private String medicalItemName;
    private Double price;
    private String isSalaryName;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private String isCalculate;
}
