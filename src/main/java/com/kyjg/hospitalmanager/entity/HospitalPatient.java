package com.kyjg.hospitalmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class HospitalPatient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String patientName;
    @Column(nullable = false, length = 13)
    private String patientPhone;
    @Column(nullable = false, length = 14)
    private String registrationNumber;
    @Column(nullable = false, length = 100)
    private String patientAddress;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String memo;
    @Column(nullable = false)
    private LocalDate dateCreate;
}
