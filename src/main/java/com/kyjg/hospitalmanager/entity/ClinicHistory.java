package com.kyjg.hospitalmanager.entity;

import com.kyjg.hospitalmanager.enums.MedicalItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class ClinicHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospitalPatientId", nullable = false)
    private HospitalPatient hospitalPatient;
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;
    @Column(nullable = false)
    private Boolean isSalary;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private LocalDate dateCure;
    @Column(nullable = false)
    private LocalTime timeCure;
    @Column(nullable = false)
    private Boolean isCalculate;
}
