package com.kyjg.hospitalmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MedicalItem {
    CHUNA("추나치료",70000, 70000),
    ACUPUNCTURE("침",20000, 3000),
    PHYSICS("물리치료",20000,5000);

    private final String name;
    private final double nonSalaryPrice;
    private final double salaryPrice;
}
