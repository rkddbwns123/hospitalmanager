package com.kyjg.hospitalmanager.controller;

import com.kyjg.hospitalmanager.entity.HospitalPatient;
import com.kyjg.hospitalmanager.model.HistoryInsuranceCorporationItem;
import com.kyjg.hospitalmanager.model.HistoryItem;
import com.kyjg.hospitalmanager.model.HistoryRequest;
import com.kyjg.hospitalmanager.service.HistoryService;
import com.kyjg.hospitalmanager.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final HistoryService historyService;
    private final PatientService patientService;
    @PostMapping("/new/patient-id/{patientId}")
    public String setHistory(@PathVariable long patientId, @RequestBody @Valid HistoryRequest request) {
        HospitalPatient hospitalPatient = patientService.getData(patientId);
        historyService.setHistory(hospitalPatient, request);

        return "OK";
    }
    @GetMapping("/all/date")
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate searchDate) {
        return historyService.getHistoriesByDate(searchDate);
    }

    @GetMapping("/all/insurance")
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getHistoriesByInsurance(searchDate);
    }
}
