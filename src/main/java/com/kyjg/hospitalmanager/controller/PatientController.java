package com.kyjg.hospitalmanager.controller;

import com.kyjg.hospitalmanager.model.PatientRequest;
import com.kyjg.hospitalmanager.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/patient")
public class PatientController {
    private final PatientService patientService;
    @PostMapping("/info")
    public String setPatient(@RequestBody @Valid PatientRequest request) {
        patientService.setPatient(request);

        return "OK";
    }
}
