package com.kyjg.hospitalmanager.repository;

import com.kyjg.hospitalmanager.entity.ClinicHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {
    List<ClinicHistory> findAllByDateCureOrderByIdDesc(LocalDate dateCure);
    List<ClinicHistory> findAllByIsSalaryAndDateCureAndIsCalculateOrderByIdDesc(boolean isSalary, LocalDate dateCure, boolean isCalculate);
}
