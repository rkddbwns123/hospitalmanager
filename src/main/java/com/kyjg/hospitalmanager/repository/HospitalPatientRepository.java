package com.kyjg.hospitalmanager.repository;

import com.kyjg.hospitalmanager.entity.HospitalPatient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalPatientRepository extends JpaRepository<HospitalPatient, Long> {
}
