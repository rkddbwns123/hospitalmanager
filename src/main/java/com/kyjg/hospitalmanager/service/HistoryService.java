package com.kyjg.hospitalmanager.service;

import com.kyjg.hospitalmanager.entity.ClinicHistory;
import com.kyjg.hospitalmanager.entity.HospitalPatient;
import com.kyjg.hospitalmanager.model.HistoryInsuranceCorporationItem;
import com.kyjg.hospitalmanager.model.HistoryItem;
import com.kyjg.hospitalmanager.model.HistoryRequest;
import com.kyjg.hospitalmanager.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalPatient hospitalPatient, HistoryRequest historyRequest) {
        ClinicHistory addData = new ClinicHistory();
        addData.setHospitalPatient(hospitalPatient);
        addData.setMedicalItem(historyRequest.getMedicalItem());
        addData.setPrice(historyRequest.getIsSalary() ? historyRequest.getMedicalItem().getSalaryPrice() : historyRequest.getMedicalItem().getNonSalaryPrice());
        addData.setIsSalary(historyRequest.getIsSalary());
        addData.setDateCure(LocalDate.now());
        addData.setTimeCure(LocalTime.now());
        addData.setIsCalculate(false);

        clinicHistoryRepository.save(addData);
    }

    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByDateCureOrderByIdDesc(searchDate);

        List<HistoryItem> result = new LinkedList<>();
        for (ClinicHistory item : originList) {
            HistoryItem addItem = new HistoryItem();

            addItem.setHistoryId(item.getId());
            addItem.setPatientId(item.getHospitalPatient().getId());
            addItem.setPatientName(item.getHospitalPatient().getPatientName());
            addItem.setPatientPhone(item.getHospitalPatient().getPatientPhone());
            addItem.setRegistrationNumber(item.getHospitalPatient().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItem().getName());
            addItem.setPrice(item.getPrice());
            addItem.setIsSalaryName(item.getIsSalary() ? "급여" : "비급여");
            addItem.setDateCure(item.getDateCure());
            addItem.setTimeCure(item.getTimeCure());
            addItem.setIsCalculate(item.getIsCalculate() ? "완료" : "대기");

            result.add(addItem);
        }
        return result;
    }
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByIsSalaryAndDateCureAndIsCalculateOrderByIdDesc(true, searchDate, true);

        List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryInsuranceCorporationItem addItem = new HistoryInsuranceCorporationItem();

            addItem.setPatientName(item.getHospitalPatient().getPatientName());
            addItem.setPatientPhone(item.getHospitalPatient().getPatientPhone());
            addItem.setRegistrationNumber(item.getHospitalPatient().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItem().getName());
            addItem.setMedicalItemNonSalaryPrice(item.getMedicalItem().getNonSalaryPrice());
            addItem.setPatientContributionPrice(item.getPrice());
            addItem.setBillingAmount(item.getMedicalItem().getNonSalaryPrice() - item.getPrice());
            addItem.setDateCure(item.getDateCure());

            result.add(addItem);
        }
        return result;
    }
}
