package com.kyjg.hospitalmanager.service;

import com.kyjg.hospitalmanager.entity.HospitalPatient;
import com.kyjg.hospitalmanager.model.PatientRequest;
import com.kyjg.hospitalmanager.repository.HospitalPatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final HospitalPatientRepository hospitalPatientRepository;

    public void setPatient(PatientRequest request) {
        HospitalPatient addData = new HospitalPatient();

        addData.setPatientName(request.getPatientName());
        addData.setPatientPhone(request.getPatientPhone());
        addData.setRegistrationNumber(request.getRegistrationNumber());
        addData.setPatientAddress(request.getPatientAddress());
        addData.setMemo(request.getMemo());
        addData.setDateCreate(LocalDate.now());

        hospitalPatientRepository.save(addData);
    }

    public HospitalPatient getData(long id) {
        return hospitalPatientRepository.findById(id).orElseThrow();
    }
}
